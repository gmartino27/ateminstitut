# remove unused content elements
TCEFORM.tt_content.CType {
	removeItems = header, text, textpic, image, bullets, table, menu_abstract, menu_categorized_content, menu_categorized_pages, menu_recently_updated, menu_related_pages
}

# Spaltenlayouts
# Eingesetzt hier: Resources/Private/Extensions/fluid_styled_content/Layouts/Default.html & Resources/Private/Extensions/gridelements/Layouts/General.html
TCEFORM.tt_content.layout {
	label = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.contentLayout.label
	altLabels {
		## full width
		1 = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.contentLayout.value1
		## full width, keep paddings
		3 = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.contentLayout.value3		
	}
	# value 2 was used in older jetpack versions (Ganze Breite ohne Lücken) -> remove gaps in gridelements
	removeItems = 2
	addItems{
		#4 = ohne Lücken mit fixer Breite (erlaubt innerhalb Spalte) -> add in grid flexform
		## smaller content width
		5 =  LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.contentLayout.value5
	}

    # Wählbare Layouts deaktivieren für Content Typ 'Datensatz einfügen' bzw. 'shortcut'
    # weil es keinen Einfluss hat, und das Layout vom Ursprungselement
    # übernommen wird.
	types.shortcut {
		disabled = 1
	}
}

# Backgrounds
# Select options are added in t8_custom
TCEFORM.tt_content.frame_class {
	label = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.background.label
	removeItems = ruler-before, ruler-after, indent, indent-left, indent-right, none

	# Wählbare Layouts deaktivieren für Content Typ 'Datensatz einfügen' bzw. 'shortcut'
    # weil es keinen Einfluss hat, und das Layout vom Ursprungselement
    # übernommen wird.
	types.shortcut {
		disabled = 1
	}
}

# Space before
TCEFORM.tt_content.space_before_class {
	keepItems =
	addItems {
		0 = LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.default_value
		padding--less-top = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.padding.less
		padding--more-top = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.padding.more
	}
}

# Space after
TCEFORM.tt_content.space_after_class {
	keepItems =
	addItems {
		0 = LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.default_value
		padding--less-bottom = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.padding.less
		padding--more-bottom = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.padding.more
	}
}

# Optionen deaktivieren
TCEFORM.tt_content {
	linkToTop.disabled = 1
    #sectionIndex.disabled = 1
	date.disabled = 1
}

# Headings:
# Rename Labels
# Add/Remove Options
TCEFORM.tt_content.header_layout {
	label = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.headerLayout.label
	altLabels {
		1 = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.headerLayout.h1
		2 = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.headerLayout.h2
		3 = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.headerLayout.h3
		4 = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.headerLayout.h4
		5 = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.headerLayout.h5
	}
	addItems {
		6 = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.headerLayout.h6
	}
	# Remove Default, Heading 1, hidden
	removeItems = 0,1,100
}

# sizes of heading fields (bc of new t8_jetpack fields)
TCEFORM.tt_content {
	header.config.size = 50
	subheader.config.size = 50
	header_layout.config.size = 1
	header_position.config.size = 1
}

# Add information to subtitle label (that subtitle will be displayed instead of title in frontend)
TCEFORM.pages.subtitle {
	label = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.pageSubtitle.label
}

# Add information to description (that it will be inherited to subpages)
TCEFORM.pages.description {
	label = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.pageMetadescription.label
}

# Rename option "Border around each element"
# is used as "force 100% image width" in Template (Resources/Private/Extensions/fluid_styled_content/Partials/Media/Rendering/Image.html)
TCEFORM.tt_content.imageborder {
    label = LLL:EXT:t8_jetpack/Resources/Private/Language/locallang.xlf:tceform.imageWidth100.label
}

# disable adding (copy XY) to header for copied elements
# https://docs.typo3.org/m/typo3/reference-tsconfig/master/en-us/PageTsconfig/TceMain.html#disableprependatcopy
TCEMAIN.table.tt_content {
	disablePrependAtCopy = 1
}

## disable Translation Mode "Translate"
mod.web_layout.localization.enableTranslate = 0