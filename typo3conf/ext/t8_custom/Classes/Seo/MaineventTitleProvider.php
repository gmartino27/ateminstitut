<?php

namespace Terminal8\T8Custom\Seo;

use TYPO3\CMS\Core\PageTitle\AbstractPageTitleProvider;

class MaineventTitleProvider extends AbstractPageTitleProvider
{
    /**
     * @param string $title
     */
    public function setTitleByMainevent(string $title)
    {
        $this->title = $title;
    }
}