<?php

namespace Terminal8\T8Custom\Hooks\Backend;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;

/**
 * Hook into PageLayoutView to hide tt_content elements in page view
 *
 */
class PageViewQueryHook
{
    protected static $count = 0;

    /**
     * Prevent inline tt_content elements in news articles from
     * showing in the page module.
     *
     * @param array $parameters
     * @param string $table
     * @param int $pageId
     * @param array $additionalConstraints
     * @param string[] $fieldList
     * @param QueryBuilder $queryBuilder
     *
     * @return void
     */
    public function modifyQuery(
        $parameters,
        $table,
        $pageId,
        $additionalConstraints,
        $fieldList,
        QueryBuilder $queryBuilder
    ): void {
        if ($table === 'tt_content' && $pageId > 0) {

            // Get page record base on page uid
            $pageRecord = BackendUtility::getRecord('pages', $pageId, 'uid', " AND doktype='254'");

            if (is_array($pageRecord)) {
                if ($pageRecord['uid'] === 19) {
                    // Only hide elements which are inline, allowing for standard
                    // elements to show
                    $queryBuilder->andWhere(
                        $queryBuilder->expr()->eq('mainevent', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
                    );

                    self::$count++;
                }
            }
        }
    }
}