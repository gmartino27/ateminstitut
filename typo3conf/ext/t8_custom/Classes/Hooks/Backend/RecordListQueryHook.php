<?php

namespace Terminal8\T8Custom\Hooks\Backend;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;

/**
 * Hook into DatabaseRecordList to hide tt_content elements in list view for mainevent records
 *
 */
class RecordListQueryHook
{
    protected static $count = 0;
    
    public function modifyQuery(
        array &$parameters,
        string $table,
        int $pageId,
        array $additionalConstraints,
        array $fieldList,
        QueryBuilder $queryBuilder
    ) {
        if ($table === 'tt_content' && $pageId > 0) {
            $pageRecord = BackendUtility::getRecord('pages', $pageId, 'uid', " AND doktype='254'");
            if (is_array($pageRecord)) {
                if ($pageRecord['uid'] === 19) {
                    $queryBuilder->where(...['1=2']);
                    self::$count++;
                }
            }
        } 
    }
}