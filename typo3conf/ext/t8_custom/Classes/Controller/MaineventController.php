<?php

namespace Terminal8\T8Custom\Controller;

use Terminal8\T8Custom\Domain\Model\Mainevent;
use Terminal8\T8Custom\Seo\MaineventTitleProvider;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * EventController
 */
class MaineventController extends AbstractController
{

    /**
     * List view
     */
    public function listAction()
    {
        if ($this->settings['queryLimit']) {
            $limit = intval($this->settings['queryLimit']);
        } else {
            $limit = 100;
        }

        $mainEvents = $this->maineventRepository->findLimited($limit);
        
        $categories = [];
        $uniqueCategories = [];
        foreach ($mainEvents as $mainEvent) {
            // Set subevents to mainevent
            /** @var Mainevent $mainEvent */
            if ($mainEvent->getRecurringevents()) {
                $this->setRecurringeventsAsObjectStorage($mainEvent);
            }

            // Take only to mainevent assigned categories for the filter
            if ($this->settings['showcategoryfilter']) {
                if (count($mainEvent->getCategory()) > 0) {
                    foreach ($mainEvent->getCategory() as $category) {
                        $categories[] = $category;
                    }
                }
            }
        }

        $this->view->assignMultiple([
            'mainEvents' => $mainEvents,
            'categories' => array_unique($categories)
        ]);
    }

    /**
     * Detail view
     *
     * @param \Terminal8\T8Custom\Domain\Model\Mainevent $mainEvent Mainevent
     * @return mixed string|void
     */
    public function detailAction(Mainevent $mainEvent = null)
    {
        /** @var MaineventTitleProvider $provider */
        $provider = GeneralUtility::makeInstance(MaineventTitleProvider::class);
        $provider->setTitleByMainevent($mainEvent->getTitle());


        if ($mainEvent->getRecurringevents()) {
            $this->setRecurringeventsAsObjectStorage($mainEvent);
        }

        $this->view->assign('mainEvent', $mainEvent);
    }
}
