<?php

namespace Terminal8\T8Custom\Controller;

use Terminal8\T8Custom\Domain\Model\Mainevent;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * AbstractController
 */
abstract class AbstractController extends ActionController
{

    /**
     * MaineventRepository
     *
     * @var \Terminal8\T8Custom\Domain\Repository\MaineventRepository
     */
    protected $maineventRepository;

    /**
     * EventRepository
     *
     * @var \DERHANSEN\SfEventMgt\Domain\Repository\EventRepository
     */
    protected $eventRepository;

    /**
     * DI for $maineventRepository
     *
     * @param \Terminal8\T8Custom\Domain\Repository\MaineventRepository $maineventRepository
     */
    public function injectMaineventRepository(\Terminal8\T8Custom\Domain\Repository\MaineventRepository $maineventRepository)
    {
        $this->maineventRepository = $maineventRepository;
    }

    /**
     * DI for $eventRepository
     *
     * @param \DERHANSEN\SfEventMgt\Domain\Repository\EventRepository $eventRepository
     */
    public function injectEventRepository(\DERHANSEN\SfEventMgt\Domain\Repository\EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * Fill objectStorage from QueryResult
     * @param Mainevent $mainEvent
     */
    public function setRecurringeventsAsObjectStorage(Mainevent $mainEvent) {
        $recurringEvents = [];
        $recurringEventsUids = GeneralUtility::trimExplode(',', $mainEvent->getRecurringevents());
        foreach ($recurringEventsUids as $recurringEventsUid) {
            $recurringEvents[] = $this->eventRepository->findByUid($recurringEventsUid);
        }

        $mainEvent->setSubevents($this->fillOjectStorageFromArray($recurringEvents));
    }

    /**
     * Fill objectStorage from QueryResult
     * @param array $array
     * @return ObjectStorage
     */
    public function fillOjectStorageFromArray(array $array = []) {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /* @var $objectStorage ObjectStorage */
        $objectStorage = $objectManager->get(ObjectStorage::class);

        if (null !== $array) {
            foreach($array AS $object) {
                $objectStorage->attach($object);
            }
        }

        return $objectStorage;
    }
}
