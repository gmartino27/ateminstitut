<?php

/*
 * This file is part of the Extension "sf_event_mgt" for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Terminal8\T8Custom\Domain\Repository;

/**
 * The repository for Mainevents
 */
class MaineventRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * Set default sorting
     *
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
    ];

    /**
     * Finds mainevents by limit
     *
     * @param int $limit
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findLimited(int $limit = 0)
    {
        $query = $this->createQuery();
        $query->setLimit($limit);

        return $query->execute();
    }
}