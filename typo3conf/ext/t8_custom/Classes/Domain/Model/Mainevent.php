<?php

namespace Terminal8\T8Custom\Domain\Model;

use TYPO3\CMS\Extbase\Annotation as Extbase;

/**
 * Mainevent
 */
class Mainevent extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * @var \DateTime
     */
    protected $tstamp;

    /**
     * @var bool
     */
    protected $hidden = false;

    /**
     * @var \DateTime
     */
    protected $starttime;

    /**
     * @var \DateTime
     */
    protected $endtime;

    /**
     * Title
     *
     * @var string
     */
    protected $title = '';

    /**
     * Contentelements
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Terminal8\T8Custom\Domain\Model\Contentelement>
     */
    protected $contentelements = '';



    /**
     * Recurring events
     *
     * @var string
     */
    protected $recurringevents = '';

    /**
     * Teaser
     *
     * @var string
     */
    protected $teaser = '';

    /**
     * Description
     *
     * @var string
     */
    protected $description = '';

    /**
     * Category
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DERHANSEN\SfEventMgt\Domain\Model\Category>
     * @Extbase\ORM\Lazy
     */
    protected $category;

    /**
     * Additional files
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @Extbase\ORM\Lazy
     */
    protected $files;

    /**
     * recurring events
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DERHANSEN\SfEventMgt\Domain\Model\Event>
     */
    protected $subevents;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->category = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->files = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->subevents = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Set time stamp
     *
     * @param \DateTime $tstamp time stamp
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * Get hidden flag
     *
     * @return bool
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set hidden flag
     *
     * @param bool $hidden hidden flag
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * Get start time
     *
     * @return \DateTime
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Set start time
     *
     * @param \DateTime $starttime start time
     */
    public function setStarttime($starttime)
    {
        $this->starttime = $starttime;
    }

    /**
     * Get endtime
     *
     * @return \DateTime
     */
    public function getEndtime()
    {
        return $this->endtime;
    }

    /**
     * Set end time
     *
     * @param \DateTime $endtime end time
     */
    public function setEndtime($endtime)
    {
        $this->endtime = $endtime;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title Title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the recurring events
     *
     * @return string $recurringevents
     */
    public function getRecurringevents()
    {
        return $this->recurringevents;
    }

    /**
     * Sets the recurring events
     *
     * @param string $recurringevents Title
     */
    public function setRecurringevents($recurringevents)
    {
        $this->recurringevents = $recurringevents;
    }

    /**
     * Returns the teaser
     *
     * @return string
     */
    public function getTeaser()
    {
        return $this->teaser;
    }

    /**
     * Sets the teaser
     *
     * @param string $teaser Teaser
     */
    public function setTeaser($teaser)
    {
        $this->teaser = $teaser;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description Description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Adds a Category
     *
     * @param \DERHANSEN\SfEventMgt\Domain\Model\Category $category Category
     */
    public function addCategory(\DERHANSEN\SfEventMgt\Domain\Model\Category $category)
    {
        $this->category->attach($category);
    }

    /**
     * Removes a Category
     *
     * @param \DERHANSEN\SfEventMgt\Domain\Model\Category $categoryToRemove The Category to be removed
     */
    public function removeCategory(\DERHANSEN\SfEventMgt\Domain\Model\Category $categoryToRemove)
    {
        $this->category->detach($categoryToRemove);
    }

    /**
     * Returns the category
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Sets the category
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $category Category
     */
    public function setCategory(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $category)
    {
        $this->category = $category;
    }

    /**
     * Adds a file
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $file File
     */
    public function addFiles(\TYPO3\CMS\Extbase\Domain\Model\FileReference $file)
    {
        $this->files->attach($file);
    }

    /**
     * Removes a file
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $fileToRemove File
     */
    public function removeFiles(\TYPO3\CMS\Extbase\Domain\Model\FileReference $fileToRemove)
    {
        $this->files->detach($fileToRemove);
    }

    /**
     * Returns the files
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage $files
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Sets the files
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $files Files
     */
    public function setFiles(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $files)
    {
        $this->files = $files;
    }

    /**
     * Returns the content elements
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Terminal8\T8Custom\Domain\Model\Contentelement> contentelements
     */
    public function getContentelements() {
        return $this->contentelements;
    }


    /**
     * Adds recurring subevents
     *
     * @param \DERHANSEN\SfEventMgt\Domain\Model\Event $subevents Events
     */
    public function addSubevents(\DERHANSEN\SfEventMgt\Domain\Model\Event $subevents)
    {
        $this->subevents->attach($subevents);
    }

    /**
     * Removes recurring subevents
     *
     * @param \DERHANSEN\SfEventMgt\Domain\Model\Event $subeventToRemove The Subevent to be removed
     */
    public function removeSubevents(\DERHANSEN\SfEventMgt\Domain\Model\Event $subeventToRemove)
    {
        $this->subevents->detach($subeventToRemove);
    }

    /**
     * Returns the subevents
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getSubevents()
    {
        return $this->subevents;
    }

    /**
     * Sets the subevents
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $subevents Subevent
     */
    public function setSubevents(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $subevents)
    {
        $this->subevents = $subevents;
    }

    /**
     * Get id list of content elements
     *
     * @return string
     */
    public function getContentElementIdList()
    {
        return $this->getIdOfContentElements();
    }

    /**
     * Collect id list
     *
     * @param bool $original
     * @return string
     */
    protected function getIdOfContentElements($original = true)
    {
        $idList = [];
        $contentElements = $this->getContentelements();
        if ($contentElements) {
            foreach ($this->getContentelements() as $contentElement) {
                $idList[] = $original ? $contentElement->getUid() : $contentElement->_getProperty('_localizedUid');
            }
        }
        return implode(',', $idList);
    }
}