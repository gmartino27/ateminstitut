<?php
namespace Terminal8\T8Custom\Domain\Model;

/**
 * Event
 */
class SfeventmgtEvent extends \DERHANSEN\SfEventMgt\Domain\Model\Event
{
    /**
     * eventtype
     *
     * @var string
     */
    protected $eventtype = '';

    /**
     * Returns the eventtype
     *
     * @return string $eventtype
     */
    public function getEventtype()
    {
        return $this->eventtype;
    }

    /**
     * Sets the eventtype
     *
     * @param string $eventtype
     * @return void
     */
    public function setEventtype($eventtype)
    {
        $this->eventtype = $eventtype;
    }
}