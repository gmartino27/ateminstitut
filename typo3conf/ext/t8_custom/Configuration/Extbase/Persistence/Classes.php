<?php
declare(strict_types=1);

return [
    \Terminal8\T8Custom\Domain\Model\Contentelement::class => [
        'tableName' => 'tt_content',
    ],
    \Terminal8\T8Custom\Domain\Model\SfeventmgtEvent::class => [
        'tableName' => 'tx_sfeventmgt_domain_model_event',
    ],
];