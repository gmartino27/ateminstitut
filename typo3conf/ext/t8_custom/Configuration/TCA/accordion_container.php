<?php
return array (
    'ctrl' =>
        array (
            'title' => 'Container',
            'label' => 'accordion_header',
            'tstamp' => 'tstamp',
            'crdate' => 'crdate',
            'cruser_id' => 'cruser_id',
            'dividers2tabs' => true,
            'versioningWS' => true,
            'languageField' => 'sys_language_uid',
            'transOrigPointerField' => 'l10n_parent',
            'transOrigDiffSourceField' => 'l10n_diffsource',
            'delete' => 'deleted',
            'enablecolumns' =>
                array (
                    'disabled' => 'hidden',
                    'starttime' => 'starttime',
                    'endtime' => 'endtime',
                ),
            'searchFields' => 'accordion_content',
            'dynamicConfigFile' => '',
            'iconfile' => 'EXT:t8_custom/Resources/Public/Icons/Content/accordion.png',
            'hideTable' => true,
        ),
    'interface' =>
        array (
            'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, accordion_content',
        ),
    'types' =>
        array (
            1 =>
                array (
                    'showitem' => 'sys_language_uid,l10n_parent,l10n_diffsource,hidden,accordion_header,accordion_content,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime,endtime',
                ),
        ),
    'palettes' =>
        array (
            1 =>
                array (
                    'showitem' => '',
                ),
        ),
    'columns' =>
        array (
            'sys_language_uid' =>
                array (
                    'exclude' => 1,
                    'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
                    'config' =>
                        array (
                            'type' => 'select',
                            'renderType' => 'selectSingle',
                            'items' =>
                                array (
                                    0 =>
                                        array (
                                            0 => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                                            1 => -1,
                                            2 => 'flags-multiple',
                                        ),
                                ),
                            'special' => 'languages',
                            'default' => 0,
                        ),
                ),
            'l10n_parent' =>
                array (
                    'displayCond' => 'FIELD:sys_language_uid:>:0',
                    'exclude' => 1,
                    'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
                    'config' =>
                        array (
                            'type' => 'select',
                            'renderType' => 'selectSingle',
                            'items' =>
                                array (
                                    0 =>
                                        array (
                                            0 => '',
                                            1 => 0,
                                        ),
                                ),
                            'foreign_table' => 'accordion_container',
                            'foreign_table_where' => 'AND accordion_container.pid=###CURRENT_PID### AND accordion_container.sys_language_uid IN (-1,0)',
                            'default' => 0,
                        ),
                ),
            'l10n_diffsource' =>
                array (
                    'config' =>
                        array (
                            'type' => 'passthrough',
                        ),
                ),
            't3ver_label' =>
                array (
                    'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
                    'config' =>
                        array (
                            'type' => 'input',
                            'size' => 30,
                            'max' => 255,
                        ),
                ),
            'hidden' =>
                array (
                    'exclude' => 1,
                    'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
                    'config' =>
                        array (
                            'type' => 'check',
                        ),
                ),
            'starttime' =>
                array (
                    'exclude' => 1,
                    'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
                    'config' =>
                        array (
                            'behaviour' =>
                                array (
                                    'allowLanguageSynchronization' => true,
                                ),
                            'renderType' => 'inputDateTime',
                            'type' => 'input',
                            'size' => 13,
                            'eval' => 'datetime,int',
                            'checkbox' => 0,
                            'default' => 0,
                        ),
                ),
            'endtime' =>
                array (
                    'exclude' => 1,
                    'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
                    'config' =>
                        array (
                            'behaviour' =>
                                array (
                                    'allowLanguageSynchronization' => true,
                                ),
                            'renderType' => 'inputDateTime',
                            'type' => 'input',
                            'size' => 13,
                            'eval' => 'datetime,int',
                            'checkbox' => 0,
                            'default' => 0,
                        ),
                ),
            'parentid' =>
                array (
                    'config' =>
                        array (
                            'type' => 'select',
                            'renderType' => 'selectSingle',
                            'items' =>
                                array (
                                    0 =>
                                        array (
                                            0 => '',
                                            1 => 0,
                                        ),
                                ),
                            'foreign_table' => 'tt_content',
                            'foreign_table_where' => 'AND tt_content.pid=###CURRENT_PID### AND tt_content.sys_language_uid IN (-1,###REC_FIELD_sys_language_uid###)',
                        ),
                ),
            'parenttable' =>
                array (
                    'config' =>
                        array (
                            'type' => 'passthrough',
                        ),
                ),
            'sorting' =>
                array (
                    'config' =>
                        array (
                            'type' => 'passthrough',
                        ),
                ),
            'accordion_header' =>
                array (
                    'config' =>
                        array (
                            'eval' => 'required',
                            'type' => 'input',
                        ),
                    'exclude' => '1',
                    'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header',
                    'order' => 3,
                ),
            'accordion_content' =>
                array (
                    'config' =>
                        array (
                            'eval' => 'required',
                            'type' => 'text',
                            'enableRichtext' => true,
                        ),
                    'exclude' => '1',
                    'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext_formlabel',
                    'order' => 2,
                ),
        ),
);