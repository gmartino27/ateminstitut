<?php
defined('TYPO3_MODE') || die();

call_user_func(function () {

    /**
     * Mainevent plugin options
     */
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'Terminal8.t8_custom',
        'Maineventplugin',
        'Mainevent'
    );

    /** Remove unused fields for mainevent plugin */
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['t8custom_maineventplugin'] = 'recursive';

    /** Add Flexform for mainevent plugin */
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['t8custom_maineventplugin'] = 'pi_flexform';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        't8custom_maineventplugin',
        'FILE:EXT:t8_custom/Configuration/FlexForms/Flexform_plugin.xml'
    );


    /**
     * Hide sf_event_mgt tables
     */
    $GLOBALS['TCA']['tx_sfeventmgt_domain_model_event']['ctrl']['hideTable'] = 1;
    $GLOBALS['TCA']['tx_sfeventmgt_domain_model_registration']['ctrl']['hideTable'] = 1;

    /**
     * Set enable registration for events as default and don't notify admin
     */
    $GLOBALS['TCA']['tx_sfeventmgt_domain_model_event']['columns']['enable_registration']['config']['default'] = 1;
    $GLOBALS['TCA']['tx_sfeventmgt_domain_model_event']['columns']['enable_registration']['config']['default'] = 1;


    /**
     * Extend sf_event_mgt with new fields
     */
    $fields = [
        'eventtype' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:t8_custom/Resources/Private/Language/locallang.xlf:sf_event_mgt.eventtype',
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'trim,required'
            ],
        ]
    ];

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_sfeventmgt_domain_model_event', $fields);
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tx_sfeventmgt_domain_model_event',
        'eventtype',
        '',
        'after:enddate'
    );


    /**
     * Accordion element
     */
    $AccordionTempColumns = array (
        'accordion_container' =>
            array (
                'config' =>
                    array (
                        'appearance' =>
                            array (
                                'enabledControls' =>
                                    array (
                                        'dragdrop' => '1',
                                    ),
                                'expandSingle' => '1',
                                'levelLinksPosition' => 'top',
                                'newRecordLinkTitle' => 'Neuer Eintrag',
                                'useSortable' => '1',
                            ),
                        'foreign_field' => 'parentid',
                        'foreign_sortby' => 'sorting',
                        'foreign_table' => 'accordion_container',
                        'foreign_table_field' => 'parenttable',
                        'type' => 'inline',
                    ),
                'exclude' => '1',
                'label' => 'Container',
            ),
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $AccordionTempColumns);
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
        'Accordion',
        '--div--',
    ];
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
        'Accordion',
        'accordion',
        'accordion',
    ];

    $accordionTempTypes = array (
        'accordion' => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,header,header_layout,accordion_container,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
        ],
    );

    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['accordion'] = 'accordion';
    $GLOBALS['TCA']['tt_content']['types'] += $accordionTempTypes;
});