<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

// Mainevent plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Terminal8.t8_custom',
    'Maineventplugin',
    [
        'Mainevent' => 'list,detail'
    ],
    // non-cacheable actions
    []
);

// Hide content elements in list module for mainevents
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][\TYPO3\CMS\Recordlist\RecordList\DatabaseRecordList::class]['modifyQuery'][]
    = \Terminal8\T8Custom\Hooks\Backend\RecordListQueryHook::class;

// Hide content elements in page module for mainevents
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][\TYPO3\CMS\Backend\View\PageLayoutView::class]['modifyQuery'][] = \Terminal8\T8Custom\Hooks\Backend\PageViewQueryHook::class;

// Add TitleProvider for mainevents
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(trim('
    config.pageTitleProviders {
        mainevent {
            provider = Terminal8\T8Custom\Seo\MaineventTitleProvider
            before = altPageTitle,record,seo
        }
    }
'));

// XCLASS sf_event_mgt
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\DERHANSEN\SfEventMgt\Domain\Model\Event::class] = [
    'className' => \Terminal8\T8Custom\Domain\Model\SfeventmgtEvent::class
];

// Register extended domain class
\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
    ->registerImplementation(
        \DERHANSEN\SfEventMgt\Domain\Model\Event::class,
        \Terminal8\T8Custom\Domain\Model\SfeventmgtEvent::class
    );

/* Settings */
// Define new RTE Config
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['T8Custom'] = 'EXT:t8_custom/Configuration/CkEditor/T8Custom.yaml';

/* Typoscript */
// add pagets
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig("@import 'EXT:t8_custom/Configuration/TypoScript/pagets/pagetsMain.typoscript'");

/**
 * Akkordeon
 */
// Register accordion content element icons
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon(
    'accordion',
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    [
        'source' => 'EXT:t8_custom/Resources/Public/Icons/Content/accordion.png',
    ]
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:t8_custom/Configuration/PageTSconfig/Accordion.typoscript">'
);

// Autoswitch to list view for folder
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/db_layout.php']['drawFooterHook']['autoswitchtolistview'] =
    \Terminal8\T8Custom\Backend\Hooks\PageLayoutControllerHook::class . '->render';

/*$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
   \TYPO3\CMS\Core\Imaging\IconRegistry::class
);
$iconRegistry->registerIcon(
   'title-on-image', // Icon-Identifier, z.B. tx-myext-action-preview
   \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
   ['source' => 'EXT:t8_custom/Resources/Public/Icons/title-on-image.svg']
);*/
